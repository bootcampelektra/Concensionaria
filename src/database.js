const mysql = require('mysql2');


const mysqlConnection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'db_concensionaria'
});


mysqlConnection.connect(function (error){
    if(error){
        console.log(error);
        return;
    }else{
        console.log("Conexión exitosa");
    }
})

module.exports = mysqlConnection;

