const express = require('express');

const app = express();

//Configuration Server
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(express.json());


//Routes
app.use(require('./routes/empleado'));
app.use(require('./routes/clientes'));
app.use(require('./routes/autos'));
app.use(require('./routes/ventas'));


app.listen(app.get('port'), () =>{
    console.log('Servidor activo en puerto '+ app.get('port'));
})