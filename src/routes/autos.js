const {
    request,
    response
} = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");


//GET

router.get("/api/autos/todos", (resquest, response) => {
    mysqlConnection.query("SELECT * FROM Autos;",
        (err, rows, fields) => {
            if (!err) {
                response.json(rows);
            } else {
                console.log(err);
            }
        });
});



router.get("/api/autos/solo/:id", (request, response) => {
    const id = request.params.id;

    mysqlConnection.query(
        "SELECT * FROM Autos WHERE idAuto = ?;",
        [id],
        (err, rows, fields) => {
            if (!err) {
                response.json(rows);
            } else {}
        }
    );
});


// POST

router.post("/api/autos/guardar/:id", (request, response) => {
    const id = request.params.id;
    const {
        modelo,
        anio,
        color,
        marca,
        precio,
        stock
    } = request.body;

    // Validaciones

    if (!modelo) {
        return response.status(401).json({
            msg: "No puede ir el dato modelo vacío"
        });
    }

    if (!isNaN(anio) === false || !anio) {
        return response.status(401).json({
            msg: "No puede ir el dato año vacío y debe ser númerico"
        });
    }


    if (!isNaN(stock) === false || !stock ) {
        return response.status(401).json({
            msg: "No puede ir el dato stock vacío y debe ser númerico"
        });
    }


    if (!color) {
        return response.status(401).json({
            msg: "No puede ir el dato color vacío "
        });
    }

    if (!marca) {
        return response.status(401).json({
            msg: "No puede ir el dato marca vacío"
        });
    }

   

    // Validar

    async function validar() {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(
                "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?",
                [id],
                (err, rows, fields) => {
                    if (!err) {
                        resolve(rows[0].cantidad);
                    } else {
                        response.json(err);
                    }
                }
            );
        })

    }

    async function agregar() {
        const cantidad = await validar();

        if (cantidad > 0) {
            mysqlConnection.query("INSERT INTO Autos (modelo, anio, color , marca, precio,  stock) VALUES (?,?,?,?,?,?) ",
                [modelo, anio, color, marca, precio, stock], (err, rows, fields) => {
                    if (!err) {
                        response.json({
                            msg: "Auto agregado"
                        })
                    } else {
                        response.json(err);

                    }
                })
        } else {
            return response.status(401).json({
                msg: "Empleado no autorizado"
            });
        }
    }
    agregar();
})




//PUT

router.put("/api/autos/modificar/:id/:idAdmin", (request, response) => {
    const id = request.params.id;
    const idAdmin = request.params.idAdmin;
    const {
        modelo,
        anio,
        color,
        marca,
        precio,
        stock
    } = request.body;



    // Validaciones

    if (!modelo) {

        return response.status(401).json({
            msg: "No puede ir el dato modelo vacío"
        });
    }

    if (!isNaN(anio) === false || !anio) {
        return response.status(401).json({
            msg: "No puede ir el dato año vacío "
        });
    }

    if (!isNaN(stock) === false || !stock) {
        return response.status(401).json({
            msg: "No puede ir el dato stock vacío "
        });
    }

    if (!color) {
        return response.status(401).json({
            msg: "No puede ir el dato color "
        });
    }

    if (!marca) {
        return response.status(401).json({
            msg: "No puede ir el dato marca vacío y debe ser númerico"
        });
    }

    if (!modelo) {
        return response.status(401).json({
            msg: "No puede ir el dato modelo vacío y debe ser númerico"
        });
    }


    async function validar() {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(
                "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?",
                [idAdmin],
                (err, rows, fields) => {
                    if (!err) {
                        resolve(rows[0].cantidad);
                    } else {
                        response.json(err);
                    }
                }
            );
        })

    }


    async function modifica() {
        const cantidad = await validar();

        if (cantidad > 0) {

            mysqlConnection.query(
                "UPDATE Autos SET modelo = ?, anio = ? ,color  = ?, marca = ?,  precio = ?, stock = ? WHERE idAuto = ?;",
                [modelo, anio, color, marca, precio, stock, id],
                (err, rows, fields) => {
                    if (!err) {

                        response.json({
                            msg: "Auto modificado"
                        })
                    } else {
                        response.json(err);
                    }
                }
            );

        } else {
            return response.status(401).json({
                msg: "Empleado no autorizado"
            });
        }


    }

    modifica();

});



//DELETE

router.delete("/api/auto/eliminar/:id/:idEmpleado", (request, response) => {

    const id = request.params.id;
    const idEmpleado = request.params.idEmpleado;


    async function validar() {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(
                "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?",
                [idEmpleado],
                (err, rows, fields) => {
                    if (!err) {
                        resolve(rows[0].cantidad);
                    } else {
                        response.json(err);
                    }
                }
            );
        })

    }


    async function eliminar() {
        const cantidad = await validar();


        if (cantidad > 0) {


            mysqlConnection.query(
                "DELETE FROM Autos WHERE idAuto = ?",
                [id],
                (err, rows, fields) => {
                    if (!err) {
                        response.json({
                            msg: "Auto Eliminado"
                        })
                    } else {
                        response.json(err);
                    }
                }
            );
        } else {
            return response.status(401).json({
                msg: "Empleado no autorizado"
            });
        }

    }
    eliminar();
});



module.exports = router;