const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

//GET

router.get("/api/ventas/todos", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Ventas;", (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/ventas/solo/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Ventas WHERE idVenta = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


router.get("/api/ventasEstado/:estado", (request, response) => {
  const estado = request.params.estado;

  mysqlConnection.query(
    "SELECT * FROM Ventas WHERE estado = ?;",
    [estado],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


router.get("/api/ventasEstado/:num", (request, response) => {
  const num = request.params.num;

  mysqlConnection.query(
    "SELECT * FROM Ventas WHERE estado = ?;",
    [estado],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


// POST

router.post("/api/ventas/guardar/:id", (request, response) => {
  const id = request.params.id;
  const { idEmpleado, idCliente, idAuto, cantidad } = request.body;

  // Validaciones

  if (!idEmpleado) {
    return response.status(401).json({
      msg: "No puede ir el dato del id del empleado, además de existir ese empleado",
    });
  }

  if (!idCliente) {
    return response.status(401).json({
      msg: "No puede ir el dato del id del cliente, además de existir ese cliente",
    });
  }

  if (!isNaN(idAuto) === false || !idAuto) {
    return response.status(401).json({
      msg: "No puede ir el dato del id del auto y debe ser númerico, además de existir ese auto",
    });
  }

  if (!isNaN(cantidad) === false || !cantidad) {
    return response.status(401).json({
      msg: "No puede ir el dato de cantidad y debe ser númerico",
    });
  }

  // Validar

  async function validar() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?",
        [id],
        (err, rows, fields) => {
          if (!err) {
            resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  // INFO AUTOS

  async function infoAutos() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT precio, stock FROM Autos WHERE idAuto = ?",
        [idAuto],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              precio: rows[0].precio,
              stock: rows[0].stock,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  async function agregar() {
    const cantidadU = await validar();
    const datos_auto = await infoAutos();

    const precio = datos_auto["precio"];
    const stock = datos_auto["stock"];

    if (cantidadU > 0 && stock > cantidad) {
      const total = precio * cantidad;

      mysqlConnection.query(
        "INSERT INTO Ventas (numEmpleado, numCliente, idAuto , cantidad, total, estado ) VALUES (?,?,?,?,?,false) ",
        [idEmpleado, idCliente, idAuto, cantidad, total],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Venta Guardada",
            });
          } else {
            response.json(err);
          }
        }
      );
    } else {
      return response.status(401).json({
        msg: "Empleado no autorizado || cantidad insuficiente",
      });
    }
  }

  agregar();
});

//PUT

router.put("/api/ventas/modificar/:id/:num", (request, response) => {
  const id = request.params.id;
  const num = request.params.num;
  const { cantidad } = request.body;

  // Validaciones

  if (!isNaN(cantidad) === false || !cantidad) {
    return response.status(401).json({
      msg: "No puede ir el dato de cantidad y debe ser númerico",
    });
  }

  // Validar

  async function validar() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Empleados, Clientes WHERE numEmpleado = ? OR numCliente = ? ;",
        [num, num],
        (err, rows, fields) => {
          if (!err) {
            resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  //INFO VENTA

  async function infoVenta() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT  idAuto, estado FROM Ventas WHERE idVenta = ?",
        [id],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              idAuto: rows[0].idAuto,
              estado: rows[0].estado,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  // INFO AUTO

  async function infoAutos() {
    const datosVenta = await infoVenta();
    const idAuto = datosVenta["idAuto"];
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT  stock, precio FROM Autos WHERE idAuto = ?",
        [idAuto],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              stock: rows[0].stock,
              precio: rows[0].precio,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  async function modifica() {
    const cantidadU = await validar();
    const datos_auto = await infoAutos();
    const datosVenta = await infoVenta();

    const estado = datosVenta["estado"];

    const stock = datos_auto["stock"];
    const precio = datos_auto["precio"];

    const total = precio * cantidad;

    if (cantidadU > 0 && stock >= cantidad && estado === 0) {
      mysqlConnection.query(
        "UPDATE Ventas SET cantidad = ?, total = ? WHERE idVenta = ?;",
        [cantidad, total, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Venta modificada",
            });
          } else {
            response.json(err);
          }
        }
      );
    } else {
      return response.status(401).json({
        msg: "Usuario no autorizado || cantidad insuficiente || Venta ya pagada",
      });
    }
  }

  modifica();
});

// Estado venta

router.put("/api/ventas/estado/:id/:idEmpleado", async (request, response) => {
  const id = request.params.id;
  const idEmpleado = request.params.idEmpleado;
  const { estado } = request.body;

  // Validaciones

  if (!isNaN(estado) === false || !/^([0-1]{1})+$/.test(estado)) {
    return response.status(401).json({
      msg: "No puede ir el dato estado vacío y debe ser númerico",
    });
  }

  // Validar

  async function validar() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?  ;",
        [idEmpleado],
        (err, rows, fields) => {
          if (!err) {
            resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  // INFO VENTA

  async function infoVenta() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT  idAuto, estado, cantidad  FROM Ventas WHERE idVenta = ?",
        [id],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              idAuto: rows[0].idAuto,
              estado: rows[0].estado,
              cantidad: rows[0].cantidad,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  // INFO AUTO

  async function infoAutos() {
    const datosVenta = await infoVenta();
    const idAuto = datosVenta["idAuto"];
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT  stock, precio FROM Autos WHERE idAuto = ?",
        [idAuto],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              stock: rows[0].stock,
              precio: rows[0].precio,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  async function modifica() {
    const cantidadU = await validar();
    return new Promise((resolve, reject) => {
      if (cantidadU > 0) {
        mysqlConnection.query(
          "UPDATE Ventas SET estado = ? WHERE idVenta = ?;",
          [estado, id],
          (err, rows, fields) => {
            if (!err) {
              resolve("OK");
            } else {
              response.json(err);
            }
          }
        );
      } else {
        return response.status(401).json({
          msg: "Usuario no autorizado ",
        });
      }
    });
  }

  async function descontar() {
    const mensaje = await modifica();

    if (mensaje === "OK") {
      const datosVenta = await infoVenta();
      const datosAuto = await infoAutos();

      const idAuto = datosVenta["idAuto"];
      const cantidad = datosVenta["cantidad"];
      const stock = datosAuto["stock"];

      const nuevoStock = stock - cantidad;

      if (stock >= cantidad) {
        mysqlConnection.query(
          "UPDATE Autos SET stock = ? WHERE idAuto = ?;",
          [nuevoStock, idAuto],
          (err, rows, fields) => {
            if (!err) {
              response.json({
                msg: "Venta Pagada y descontada",
              });
            } else {
              response.json(err);
            }
          }
        );
      } else {
        response.json({
          msg: "Cantidad insuficiente",
        });
      }
    } else {
      response.json({
        msg: "Error",
      });
    }
  }

  // AUMENTAR

  async function aumentar() {
    const mensaje = await modifica();

    if (mensaje === "OK") {
      const datosVenta = await infoVenta();
      const datosAuto = await infoAutos();

      const idAuto = datosVenta["idAuto"];
      const cantidad = datosVenta["cantidad"];
      const stock = datosAuto["stock"];

      const nuevoStock = parseInt(stock) + parseInt(cantidad);

      mysqlConnection.query(
        "UPDATE Autos SET stock = ? WHERE idAuto = ?;",
        [nuevoStock, idAuto],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Venta Cancelada y aumentada",
            });
          } else {
            response.json(err);
          }
        }
      );
    } else {
      response.json({
        msg: "Error",
      });
    }
  }

  const datos = await infoVenta();
  const estadoVenta = datos["estado"];

  if (estado === 1) {
    if (estado != estadoVenta) {
      descontar();
    } else {
      return response.status(401).json({
        msg: "No puede pagar una cuenta ya pagada de nuevo",
      });
    }
  } else {
    if (estado != estadoVenta) {
      aumentar();
    } else {
      return response.status(401).json({
        msg: "No puede cancelar una cuenta que no ha sido pagada",
      });
    }
  }
});

//DELETE

router.delete("/api/venta/eliminar/:id/:idEmpleado", (request, response) => {
  const id = request.params.id;
  const idEmpleado = request.params.idEmpleado;

  // VALIDAR

  async function validar() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Empleados WHERE numEmpleado = ?",
        [idEmpleado],
        (err, rows, fields) => {
          if (!err) {
            resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  // INFO VENTA

  async function infoVenta() {
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT   estado FROM Ventas WHERE idVenta = ?",
        [id],
        (err, rows, fields) => {
          if (!err) {
            resolve({
              estado: rows[0].estado,
            });
          } else {
            response.json(err);
          }
        }
      );
    });
  }

  async function eliminar() {
    const cantidad = await validar();
    const datosVenta = await infoVenta();

    const estado = datosVenta["estado"];
    if (cantidad > 0 && estado === 0) {
      mysqlConnection.query(
        "DELETE FROM Ventas WHERE idVenta = ?",
        [id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Venta  Eliminada",
            });
          } else {
            response.json(err);
          }
        }
      );
    } else {
      return response.status(401).json({
        msg: "Empleado no autorizado || No se puede eliminar un venta realizada",
      });
    }
  }

  eliminar();
});

module.exports = router;
