const {
  request,
  response
} = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

//GET

router.get("/api/empleado/todos", (resquest, response) => {
  mysqlConnection.query("SELECT b.idEmpleado, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.numEmpleado, a.habilitado  FROM Empleados as b  INNER JOIN Personas as a  ON b.idEmpleado = a.idPersona;",
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
        console.log(err);
      }
    });
});


router.get("/api/empleado/solo/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idEmpleado, a.nombre, a.apellido, a.telefono,  a.edad, a.correo,  b.numEmpleado, a.habilitado  FROM Empleados as b  INNER JOIN Personas as a  ON b.idEmpleado = a.idPersona WHERE b.idEmpleado = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {}
    }
  );
});


router.get("/api/empleadosEstado/:estado", (request, response) => {
  const estado = request.params.estado;

  mysqlConnection.query(
    "SELECT b.idEmpleado, a.nombre, a.apellido, a.telefono,  a.edad, a.correo,  b.numEmpleado, a.habilitado  FROM Empleados as b  INNER JOIN Personas as a  ON b.idEmpleado = a.idPersona WHERE a.habilitado = ?;",
    [estado],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {}
    }
  );
});

//POST

router.post("/api/empleado/guardar", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numEmpleado = request.body.numEmpleado;



  // Validaciones

  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }


  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }


  if (!numEmpleado) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }


  mysqlConnection.query(
    "INSERT INTO Personas (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
    [nombre, apellido, edad, telefono, correo],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;

        mysqlConnection.query(
          "INSERT INTO Empleados (idEmpleado, numEmpleado) VALUES (?,?);",
          [id, numEmpleado],
          (err, rows, fields) => {
            if (!err) {
              response.json({
                msg: "Empleado agregado con éxito"
              });
            } else {
              response.json(err);
            }
          }
        );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/empleado/modificar/:id", (request, response) => {
  
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numEmpleado = request.body.numEmpleado;



  // Validaciones

  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }


  if (!numEmpleado) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }

  mysqlConnection.query(
    "UPDATE Personas SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id],
    (err, rows, fields) => {
      if (!err) {


        mysqlConnection.query(
          "UPDATE Empleados SET  numEmpleado = ? WHERE idEmpleado = ?;",
          [numEmpleado, id],
          (err, rows, fields) => {
            if (!err) {
              response.json({
                msg: "Empleado modificado con éxito"
              });
            } else {
              response.json(err);
            }
          }
        );
      } else {
        response.json(err);
      }
    }
  );



});

// DESACTIVAR/ACTIVAR

router.put("/api/empleado/estado/:id", (request, response) => {
  const id = request.params.id;
  const habilitado = request.body.habilitado;

  if (!isNaN(habilitado) === false || !/^([0-1]{1})+$/.test(habilitado)) {

    return response.status(401).json({
      msg: "No puede ir el dato habilitado vacío y debe ser númerico"
    });
  }

  mysqlConnection.query(
    "UPDATE Personas SET habilitado = ? WHERE idPersona = ?",
    [habilitado, id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Empleado Modificado"
        })
      } else {
        response.json(err);
      }
    }
  );
});



//DELETE

router.delete("/api/empleado/eliminar/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Personas WHERE idPersona = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Empleado Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  );
});



module.exports = router;